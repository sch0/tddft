#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include<sstream>
#include<cstdlib>
#include<cstdio>
#include<cmath>
#include<iomanip>

using namespace std;

int main(int argc,char *argv[])
{
  if(argc != 2){
    cout << "Usage: ./hogehoge time_scale" << endl;
    exit(1);
  }
  FILE *fp;
  fp = fopen("multipoles_orig","r");
  
  if(fp==NULL){
    system("cp multipoles multipoles_orig");
  }else{
    system("cp multipoles_orig multipoles");
    fclose(fp);
  }
  
  ifstream ifs("multipoles");
  ofstream ofs("multipoles_temp");

  int i;
  string sLine;
  double scale = atof(argv[1]);

  for(i=0;i<16;i++){
    getline(ifs,sLine);
    ofs << sLine << endl;
  }

  vector<int> vIter;
  vector<double> vT,vEC,vX,vY,vZ;

  while(getline(ifs,sLine)){
    istringstream isTemp(sLine);
    string sComp;
    stringstream ssDas;
    int iVal;
    double dVal;
    sComp = '\0';
    while(*sComp.c_str() == '\0'){
      getline(isTemp,sComp,' ');
    }
    ssDas << sComp;
    ssDas >> iVal;
    ssDas.clear();
    ssDas.str("");
    vIter.push_back(iVal);
    sComp = '\0';
    while(*sComp.c_str() == '\0'){
      getline(isTemp,sComp,' ');
    }
    ssDas << sComp;
    ssDas >> dVal;
    ssDas.clear();
    ssDas.str("");
    vT.push_back(dVal);
    sComp = '\0';
    while(*sComp.c_str() == '\0'){
      getline(isTemp,sComp,' ');
    }
    ssDas << sComp;
    ssDas >> dVal;
    ssDas.clear();
    ssDas.str("");
    vEC.push_back(dVal);
    sComp = '\0';
    while(*sComp.c_str() == '\0'){
      getline(isTemp,sComp,' ');
    }
    ssDas << sComp;
    ssDas >> dVal;
    ssDas.clear();
    ssDas.str("");
    vX.push_back(dVal);
    sComp = '\0';
    while(*sComp.c_str() == '\0'){
      getline(isTemp,sComp,' ');
    }
    ssDas << sComp;
    ssDas >> dVal;
    ssDas.clear();
    ssDas.str("");
    vY.push_back(dVal);
    sComp = '\0';
    while(*sComp.c_str() == '\0'){
      getline(isTemp,sComp,' ');
    }
    ssDas << sComp;
    ssDas >> dVal;
    ssDas.clear();
    ssDas.str("");
    vZ.push_back(dVal);
  }
  //double T = vT[vT.size()-1];
  double T = 53.4*41.34;
  double tau = 3*T/4;
  vector<double> sX,sY,sZ;
  int iTau,iT;
  for(i=0;i<vT.size();++i){
    if(vT[i] < tau){
      iTau = i;
    }
    if(vT[i] < T){
      iT = i;
    }
  }
  for(i=0;i<iTau;++i){
    sX.push_back(vX[i]);
    sY.push_back(vY[i]);
    sZ.push_back(vZ[i]);
  }
  for(i=iTau;i<iT;++i){
    sX.push_back(vX[i]*pow( sin( 3.14 * (T-vT[i])/(2*(T-tau)) ) , 2.0 ) );
    sY.push_back(vY[i]*pow( sin( 3.14 * (T-vT[i])/(2*(T-tau)) ) , 2.0 ) );
    sZ.push_back(vZ[i]*pow( sin( 3.14 * (T-vT[i])/(2*(T-tau)) ) , 2.0 ) );
  }
  for(i=iT;i<vT.size();++i){
    sX.push_back(0.0);
    sY.push_back(0.0);
    sZ.push_back(0.0);
  }
  for(i=0;i<vT.size();++i){
    ofs << vIter[i] << '\t';
    ofs << scientific << setprecision(12) << vT[i]*scale
    << '\t' << vEC[i] << '\t' << sX[i] << '\t' << sY[i]
    << '\t' << sZ[i] << endl;
  }
  system("cp -f multipoles_temp multipoles");
  system("rm -f multipoles_temp");
  return 0;
}
